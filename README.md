# TechChallenge 2012

### Snappers Solver

This is our teams entry for the [Clarity Consulting](http://blogs.claritycon.com "Clarity Consulting Blog") TechChallenge 2012; a solver for the game [Snappers](http://itunes.apple.com/us/app/snappers-hd/id486843920?mt=8 "Snappers HD for IPad") on IPhone or IPad.

## Team Biblical Legends

[David Johnson](http://blogs.claritycon.com/blog/author/djohnson/ "David Johnson's Blog")

[Jacob Gable](http://jacob4u2.posterous.com "Jacob Gable's Blog")

Matthew Seman