﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snappers.Core;

namespace TechChallenge.Tests
{
    [TestClass()]
    [Ignore()]
    public class BoardImageReaderTest
    {
        [TestMethod()]
        public void ShouldReadBoardFromIPadSnapshot_1_2()
        {
            CommonImageReaderTest("1-2.png", "__r_r|_r_r_|_____|__r__|r__r_|_r__r", 1);            
        }

        [TestMethod]
        public void ShouldReadBoardFromIPadSnapshot_1_9()
        {
            CommonImageReaderTest("1-9.png", "g_r_g|_rgr_|_____|r_b_r|g___g|_ror_", 3);
        }

        [TestMethod]
        public void ShouldReadBoardFromIPadSnapshot_1_19()
        {
            CommonImageReaderTest("1-19.png", "r_o_r|rrgrr|rbbbr|gg_gg|gr_rg|__r__", 2);
        }

        [TestMethod]
        public void ShouldReadBoardFromIPhoneHighSnapshot_1_9()
        {
            CommonImageReaderTest("iPhone-1-9-high.png", "g_r_g|_rgr_|_____|r_b_r|g___g|_ror_", 3);
        }

        [TestMethod]
        public void ShouldReadBoardFromIPhoneMidSnapshot_1_9()
        {
            CommonImageReaderTest("iPhone-1-9-mid.png", "g_r_g|_rgr_|_____|r_b_r|g___g|_ror_", 3);
        }

        [TestMethod]
        public void ShouldReadBoardFromIPhoneLowSnapshot_1_9()
        {
            CommonImageReaderTest("iPhone-1-9-low.png", "g_r_g|_rgr_|_____|r_b_r|g___g|_ror_", 3);
        }

        protected void CommonImageReaderTest(string imageName, string board, int numTouches)
        {
            var target = new BoardImageReader();

            var result = target.ReadBoard(imageName, numTouches);

            var expected = SnapperBoard.Make(board, numTouches);

            result.Should().Be(expected);
        }
    }
}
