﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snappers.Core;

namespace TechChallenge.Tests
{
    [TestClass]
    public class ActualGameTests : SolverTestBase
    {
        [TestMethod]
        public void GameBoard_1_5()
        {
            CommonBoardSolve("_rrr_|rr_rr|br_rb|_r_r_|_____|_____", 2);            
        }

        [TestMethod]
        public void GameBoard_1_9()
        {
            CommonBoardSolve("g_r_g|_rgr_|_____|r_b_r|g___g|_ror_", 3);
        }

        [TestMethod]
        public void GameBoard_1_19()
        {
            CommonBoardSolve("r_o_r|rrgrr|rbbbr|gg_gg|gr_rg|__r__", 2);
        }
    }
}
