﻿using System.Diagnostics;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snappers.Core;

namespace TechChallenge.Tests
{
    [TestClass]
    public class TechChallenge2012Tests : SolverTestBase
    {
        [TestMethod]
        public void FinalBoard1()
        {
            var board = SnapperBoard.Make("r_r_r|rrrrr|_rbr_|r_g_r|__b__|rrrrr", 1);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard2()
        {
            // Don't know if the number of taps is correct here.
            var board = SnapperBoard.Make("rr_rr|_r_r_|__o__|rrrrr|r_o_r|grrrg", 2);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard3()
        {
            // Not sure if this or the 4th one are incorrect boards.
            var board = SnapperBoard.Make("g__r_|_r_gg|r_r__|__rg_|__g__|_____", 2);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard4()
        {
            var board = SnapperBoard.Make("g__r_|_r_gg|r_r__|__rg_|__g__|_____", 2);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard5()
        {
            var board = SnapperBoard.Make("ggogg|ogggo|_rgr_|r_b_r|o___o|_ogo_", 3);

            CommonBoardSolve(board);
        }

        [TestMethod]
        [Ignore()]
        public void FinalBoard6()
        {
            var board = SnapperBoard.Make("o_g_o|r_o_r|rbrbr|_r_r_|g___g|gr_rg", 2);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard6_FindMinimumSolution()
        {
            var board = SnapperBoard.Make("o_g_o|r_o_r|rbrbr|_r_r_|g___g|gr_rg", 2);

            CommonBoardSolve(board, true, false);
        }

        [TestMethod]
        [Ignore()]
        public void FinalBoard7()
        {
            var board = SnapperBoard.Make("g_r__|_br_r|grr_o|bgbrr|bbrgr|_or_g", 2);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard7_FindMinimumSolution()
        {
            var board = SnapperBoard.Make("g_r__|_br_r|grr_o|bgbrr|bbrgr|_or_g", 2);

            CommonBoardSolve(board, true, false);
        }

        [TestMethod]
        public void FinalBoard8()
        {
            var board = SnapperBoard.Make("ro_or|rbgbr|o___o|brgrb|__r__|rr_rr", 4);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard9()
        {
            var board = SnapperBoard.Make("bb_r_|g_r__|rgg__|rgg__|g_r__|bb_r_", 5);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard10()
        {
            var board = SnapperBoard.Make("_____|__r__|rogor|rogor|__r__|_____", 4);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard11()
        {
            var board = SnapperBoard.Make("_g_g_|__r__|go_og|r_g_r|g___g|r___r", 6);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void FinalBoard12()
        {
            var board = SnapperBoard.Make("g___g|r_g_r|rgbgr|_bgb_|r_r_r|__g__", 6);

            CommonBoardSolve(board);
        }
    }
}
