﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snappers.Core;

namespace TechChallenge.Tests
{
    [TestClass]
    public class SolverTests : SolverTestBase
    {
        [TestMethod]
        public void ShouldWin1TouchBoard()
        {
            var board = SnapperBoard.Make("__r_r|_r_r_|_____|__r__|r__r_|_r__r", 1);

            CommonBoardSolve(board);
        }

        [TestMethod]
        public void ShouldWin3TouchBoard()
        {
            var board = SnapperBoard.Make("gr_rg|_g_g_|_rgr_|_brb_|r___r|_____", 3);

            CommonBoardSolve(board);
        }
    }
}
