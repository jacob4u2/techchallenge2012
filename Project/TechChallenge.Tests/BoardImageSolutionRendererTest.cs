﻿using Snappers.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Collections.Generic;
using FluentAssertions;
using System.Linq;
using System.Drawing.Imaging;

namespace TechChallenge.Tests
{
    
    
    /// <summary>
    ///This is a test class for BoardImageSolutionRendererTest and is intended
    ///to contain all BoardImageSolutionRendererTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoardImageSolutionRendererTest
    {

        [TestMethod()]
        [Ignore()]
        public void RenderSolutionImageTest()
        {
            var target = new BoardImageSolutionRenderer(); // TODO: Initialize to an appropriate value
            using (var boardImg = (Bitmap)Bitmap.FromFile("1-19.png"))
            {
                var board = new BoardImageReader().ReadBoard(boardImg);
                var solution = new Solver().TryToWin(board, true).FirstOrDefault();

                solution.Should().NotBeNull().And.NotBeEmpty();

                using (var solutionImg = target.RenderSolutionImage(boardImg, solution))
                {
                    solutionImg.Should().NotBeNull();

                    solutionImg.Save(@"1-19_solved.png", ImageFormat.Png);
                }
            }
        }
    }
}
