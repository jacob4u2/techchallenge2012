﻿using FluentAssertions;
using Snappers.Core;
using System.Diagnostics;

namespace TechChallenge.Tests
{
    public class SolverTestBase
    {

        protected void CommonBoardSolve(string board, int taps)
        {
            var made = SnapperBoard.Make(board, taps);

            CommonBoardSolve(made);
        }

        protected void CommonBoardSolve(SnapperBoard board, bool findShortest = false, bool returnFirst = true)
        {
            var origBoard = SnapperBoard.Clone(board);
            var solver = new Solver();
            var results = solver.TryToWin(board, findShortest, returnFirst);

            results.Should().NotBeNull("solver should find a solution").And.NotBeEmpty();

            int i = 0;
            results.ForEach(r =>
                {
                    Debug.WriteLine("Found Solution: " + i);
                    r.ForEach(x => Debug.WriteLine(string.Format("({0}, {1})", x.Item1, x.Item2)));

                    origBoard = SnapperBoard.Clone(board);
                    // Each touch should be an actual snapper.
                    r.ForEach(pos => origBoard.DoTouch(pos.Item1, pos.Item2).Should().BeTrue("should only touch spots with snappers"));
                    
                    // The board should be won.
                    origBoard.IsWon().Should().BeTrue("should have cleared all snappers from board");
                    i++;
                });

            
        }
    }
}
