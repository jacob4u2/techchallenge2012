﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Snappers.Core;

namespace TechChallenge.Tests
{
    
    
    /// <summary>
    ///This is a test class for SnapperBoardTest and is intended
    ///to contain all SnapperBoardTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SnapperBoardTest
    {
        private SnapperBoard board;

        [TestInitialize]
        public void BeforeEachTest()
        {
            // recreate the 1-3 level board.
            board = SnapperBoard.Make("_rrr_|rr_rr|br_rb|_r_r_|_____|_____", 2);
        }

        [TestMethod]
        public void ShouldMakeBoardCorrect_Board3()
        {
            board[0, 0].Should().BeNull();
            
            board[0, 1].Should().NotBeNull();
            board[0, 1].Pos.Item1.Should().Be(0);
            board[0, 1].Pos.Item2.Should().Be(1);

            board[0, 2].Should().NotBeNull();
            board[0, 2].Pos.Item1.Should().Be(0);
            board[0, 2].Pos.Item2.Should().Be(2);

            board[0, 1].State.Should().Be(SnapperColor.red);
            board[0, 2].State.Should().Be(SnapperColor.blue);
        }

        [TestMethod]
        public void ShouldNotReturnNullOnDoTouch()
        {
            board[2, 0].State.Should().Be(SnapperColor.red);

            var result = board.DoTouch(2, 0);

            board[2, 0].Should().BeNull();

            result.Should().BeTrue();
        }

        [TestMethod]
        public void ShouldHaveCorrectBoardAfterTouch()
        {
            board.DoTouch(2, 0);

            var afterBoard = SnapperBoard.Make("_____|_____|r___r|_____|_____|_____", 1);

            board.Should().Be(afterBoard);
        }

        [TestMethod]
        public void WhenTwoDartsHitASnapperTheyShouldBothBeRemoved()
        {
            board.DoTouch(0, 1);

            var afterBoard = SnapperBoard.Make("_____|_____|r___g|_____|_____|_____", 1);

            board.Should().Be(afterBoard);
        }

        [TestMethod]
        public void ShouldReturnEligibleSnappers()
        {
            var snaps = board.GetEligibleSnappers();

            snaps.Any(x => x.Pos.Item1 == 2 && x.Pos.Item2 == 0).Should().BeTrue();
        }

        [TestMethod]
        public void ShouldCloneBoard()
        {
            var boardOrig = board;

            var boardClone = SnapperBoard.Clone(boardOrig);

            for (var i = 0; i < 5; i++)
            {
                for (var j = 0; j < 6; j++)
                {
                    boardOrig[i, j].Should().Be(boardClone[i, j]);
                }
            }
        }

        [TestMethod]
        public void ShouldGetAllNodes()
        {
            board.GetAll().Should().NotBeNull().And.NotBeEmpty();

            var states = board.GetAll().Select(y => y.State).Distinct().ToList();

            states.Should().Contain(SnapperColor.blue).And.Contain(SnapperColor.red);
        }

        [TestMethod()]
        public void IsWonTest()
        {
            var notDoneBoard = board;

            notDoneBoard.IsWon().Should().BeFalse();

            var doneBoard = SnapperBoard.Make("_____|_____|_____|_____|_____|_____", 1);

            doneBoard.IsWon().Should().BeTrue();
        }
    }
}
