﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Management;

namespace Snappers.Web.Common
{
    public class LogEvent : WebRequestErrorEvent
    {
        public LogEvent(string message)
            : base(null, null, 100001, new Exception(message))
        { }
    }

    public static class Logging
    {
        public static void LogMessage(string message)
        {
            new LogEvent(message).Raise();
        }
    }
}