﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using Snappers.Core;
using RestSharp;
using System.IO;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Snappers.Web.Common;

namespace Snappers.Web.Controllers
{
    public class MailGunMessageInfo
    {
        public string From { get; set; }
        public string sender { get; set; }
        public string To { get; set; }
        public int attachment_count { get; set; }
        public string Subject { get; set; }        
    }

    public class MailGunSolutionSender
    {
        public void SendSolution(Bitmap solution, string replyTo, string subject)
        {
            RestClient client = new RestClient();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               "key-2hj5k6w62qq7l1f-21j-hceds61llg35");
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                                 "snapperssolver.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Snappers Solver <solutions@snapperssolver.mailgun.org>");
            request.AddParameter("to", replyTo);
            request.AddParameter("subject", "RE: " + subject);
            request.AddParameter("text", "Here is your solution!\r\n\r\n Thanks for using Snappers Solutions!\r\n(http://snapperssolver.apphb.com/)");
            request.AddParameter("html", "<html>Here is your solution!<br /><br />Thanks for using <a href='http://snapperssolver.apphb.com/'>Snappers Solutions</a>!</html>");
            request.AddFile("attachment", ImageToBytes(solution),"solution.png");
            
            request.Method = Method.POST;

            client.Execute(request);
        }

        public void SendProblem(string replyTo, string subject)
        {
            RestClient client = new RestClient();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               "key-2hj5k6w62qq7l1f-21j-hceds61llg35");
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
                                 "snapperssolver.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Snappers Solver <solutions@snapperssolver.mailgun.org>");
            request.AddParameter("to", replyTo);
            request.AddParameter("subject", "RE: " + subject);
            request.AddParameter("text", "Sorry, there was a problem finding a solution to your board.  Snappers Solver only supports IPad and IPhone 4 pictures currently.\r\n\r\n Thanks for using Snappers Solutions!\r\n(http://snapperssolver.apphb.com/)");
            request.AddParameter("html", "<html>Sorry, there was a problem finding a solution to your board.  Snappers Solver only supports IPad and IPhone 4 pictures currently.<br /><br />Thanks for using <a href='http://snapperssolver.apphb.com/'>Snappers Solutions</a>!</html>");
            
            request.Method = Method.POST;

            client.Execute(request);
        }

        private byte[] ImageToBytes(Bitmap solution)
        {
            var result = new byte[] { };
            using (var stream = new MemoryStream())
            {
                solution.Save(stream, ImageFormat.Png);

                stream.Flush();

                result = stream.ToArray();
            }

            return result;
        }
    }

    public class SolutionController : AsyncController
    {
        public SolutionController()
        {
            ValidateRequest = false;
        }

        [HttpPost]
        [ValidateInput(false)]
        public void EmailAsync(string sender, string subject, string from)
        {
            AsyncManager.Parameters["result"] = false;

            // Parse the email for senders address, and attachments.
            var replyTo = sender;
            if (string.IsNullOrWhiteSpace(replyTo))
            {
                Logging.LogMessage("No replyTo passed to Email Action");
                return;
            }
            
            // Get the first attachment (Request.Files), try to make it into an image
            if (Request.Files.Count < 1)
            {
                Logging.LogMessage("No attachments passed to Email Action");
                return;
            }

            AsyncManager.OutstandingOperations.Increment();
            Task.Factory.StartNew(state =>
                {
                    var attachment = state as HttpPostedFileBase;
                    try
                    {
                        using (var img = new Bitmap(attachment.InputStream))
                        {
                            var reader = new BoardImageReader();
                            var board = reader.ReadBoard(img);

                            var solution = new Solver().TryToWin(board, findShortest: true).FirstOrDefault();
                            
                            if (solution == null)
                                return;

                            using (var solutionImg = new BoardImageSolutionRenderer().RenderSolutionImage(img, solution))
                            {
                                if (string.IsNullOrWhiteSpace(subject))
                                    subject = "Your Snappers Solution";

                                new MailGunSolutionSender().SendSolution(solutionImg, replyTo, subject);
                            }
                        }
                        Logging.LogMessage("Message Sent: " + replyTo);
                        AsyncManager.Parameters["result"] = true;                        
                    }
                    catch (Exception ex)
                    {
                        Logging.LogMessage(ex.Message);
                        try
                        {
                            new MailGunSolutionSender().SendProblem(replyTo, subject);
                        }
                        catch { }
                    }

                    AsyncManager.OutstandingOperations.Decrement();
                }, Request.Files[0]);
            
        }

        public ActionResult EmailCompleted(bool result)
        {
            return result ? Content("OK") : Content("BAD");
        }
    }
}
