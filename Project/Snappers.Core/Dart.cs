﻿using System;

namespace Snappers.Core
{
    public enum Direction
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    }

    public class Dart
    {
        public Tuple<int, int> StartPos { get; set; }
        public Direction Dir { get; set; }
        public Tuple<int, int> CurrPos { get; set; }

        public bool Move()
        {
            if (Dir == Direction.Left)
                CurrPos = Tuple.Create(CurrPos.Item1 - 1, CurrPos.Item2);
            else if (Dir == Direction.Right)
                CurrPos = Tuple.Create(CurrPos.Item1 + 1, CurrPos.Item2);
            else if (Dir == Direction.Up)
                CurrPos = Tuple.Create(CurrPos.Item1, CurrPos.Item2 - 1);
            else
                CurrPos = Tuple.Create(CurrPos.Item1, CurrPos.Item2 + 1);

            return CheckInBounds();
        }

        public bool CheckInBounds()
        {
            if (CurrPos.Item1 < 0 || CurrPos.Item2 < 0 || CurrPos.Item1 > 4 || CurrPos.Item2 > 5)
                return false;
            return true;
        }
    }
}
