﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Snappers.Core
{
    public class Solver
    {
        private HashSet<SnapperBoard> NoSolutionBoards;
        private Dictionary<SnapperBoard, List<List<Tuple<int, int>>>> SolutionBoards;

        public List<List<Tuple<int, int>>> TryToWin(SnapperBoard board, bool findShortest = false, bool returnFirst = true)
        {
            NoSolutionBoards = new HashSet<SnapperBoard>();
            SolutionBoards = new Dictionary<SnapperBoard, List<List<Tuple<int, int>>>>();
            
            List<SnapperBoard> boardStates = new List<SnapperBoard>();
            List<Tuple<int, int>> attemptSpots = new List<Tuple<int, int>>();

            if (findShortest)
            {
                board.TouchesLeft = 1;
            }

            boardStates.Add(board);
            
            List<List<Tuple<int, int>>> finalSpots = TryToRecur(boardStates, attemptSpots, returnFirst);

            if (findShortest)
            {
                while (finalSpots == null)
                {
                    board.TouchesLeft++;
                    attemptSpots = new List<Tuple<int, int>>();
                    finalSpots = TryToRecur(boardStates, attemptSpots, returnFirst);
                }
            }

            return finalSpots;
        }

        private List<List<Tuple<int, int>>> TryToRecur(List<SnapperBoard> boardStates, List<Tuple<int, int>> attemptSpots, bool returnFirst = true)
        {
            List<List<Tuple<int, int>>> tmpLst;
            Tuple<int, int> tmpSpot;
            bool touchStatus;

            var origBoard = boardStates[boardStates.Count - 1];
            if (!SolutionBoards.ContainsKey(origBoard))
                SolutionBoards.Add(origBoard, new List<List<Tuple<int, int>>>());

            SnapperBoard currentBoard = SnapperBoard.Clone(origBoard);
            List<Snapper> eligibleSnaps = currentBoard.GetEligibleSnappers();

            if (currentBoard.TouchesLeft == 0)
                return null;

            foreach (var snap in eligibleSnaps) 
            {
                tmpSpot = snap.Pos;
                
                // Reset the current board
                currentBoard = SnapperBoard.Clone(origBoard);
                touchStatus = currentBoard.DoTouch(tmpSpot.Item1, tmpSpot.Item2);

                // If we've solved this board before, return it.
                if (SolutionBoards.ContainsKey(currentBoard) && SolutionBoards[currentBoard].Count > 0)
                {
                    return SolutionBoards[currentBoard];
                }

                // Skip boards we've already solved and ones that have lost.
                if (NoSolutionBoards.Contains(currentBoard)
                    || (currentBoard.TouchesLeft == 0 && !currentBoard.IsWon())) 
                {
                    continue;
                }

                attemptSpots.Add(tmpSpot);

                // If we won the board, memo our spots
                if (currentBoard.IsWon()) 
                {
                    // Add to our solved boards, maybe in the future we could get all solutions for a board.
                    SolutionBoards[origBoard].Add(attemptSpots.Select(x => x).ToList());

                    if (returnFirst)
                        return SolutionBoards[origBoard];

                    // Remove the last spot and try the next spot.
                    attemptSpots.Remove(tmpSpot);
                    continue;
                }

                boardStates.Add(currentBoard);
                
                // Try to solve the board after we touched it.
                tmpLst = TryToRecur(boardStates, attemptSpots, returnFirst);

                if (tmpLst != null)
                {
                    // Add to our solved boards                    
                    SolutionBoards[origBoard].AddRange(tmpLst.Select(x => x.Select(y => y).ToList()));

                    if (returnFirst)
                        return SolutionBoards[origBoard];                    
                }
                else
                {
                    // Add that board to the NoSolutions hash.
                    NoSolutionBoards.Add(SnapperBoard.Clone(currentBoard));
                }

                // Remove the board and spotfrom the states.
                boardStates.Remove(boardStates[boardStates.Count-1]);
                attemptSpots.Remove(tmpSpot);
            }

            return SolutionBoards.ContainsKey(origBoard) && SolutionBoards[origBoard].Count > 0 ? SolutionBoards[origBoard] : null;
        }
    }
}
