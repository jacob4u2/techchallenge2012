﻿using System;

namespace Snappers.Core
{
    public class Snapper
    {
        public SnapperColor State { get; set; }
        public Tuple<int, int> Pos { get; set; }

        public bool incrementColor()
        {
            var val = false;
            if (this.State == SnapperColor.red)
                val = true;
            
            State++;
            return val;
        }

        public static Snapper Create(SnapperColor c, int x, int y)
        {
            return new Snapper()
            {
                State = c,
                Pos = Tuple.Create(x, y)
            };
        }

        private const string form = "{0} : ({1},{2})";
        public override string ToString()
        {
            return string.Format(form, this.State, this.Pos.Item1, this.Pos.Item2);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Snapper))
                return false;

            return this.GetHashCode() == ((Snapper)obj).GetHashCode();
        }
    }

    public enum SnapperColor { blue, orange, green, red, explode };
}
