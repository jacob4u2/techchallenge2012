﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snappers.Core
{
    public class BoardBuilder
    {
        public List<List<Snapper>> ParseString(string boardString)
        {
            var rows = boardString.Split('|').ToList();
            var board = new List<List<Snapper>>();
            var row = new List<Snapper>();
            for (int i = 0; i < 6; i++) //columns 6
            {

                for (int j = 0; j < 5; j++) //rows 5
                {

                    switch (rows[i][j])
                    {
                        case '_':
                            row.Add(null);
                            break;
                        case 'r':
                            row.Add(SnapperBuilder.makeSnapper(i, j, SnapperColor.red));
                            break;
                        case 'b':
                            row.Add(SnapperBuilder.makeSnapper(i, j, SnapperColor.blue));
                            break;
                        case 'g':
                            row.Add(SnapperBuilder.makeSnapper(i, j, SnapperColor.green));
                            break;
                        case 'o':
                            row.Add(SnapperBuilder.makeSnapper(i, j, SnapperColor.orange));
                            break;
                    }
                } board.Add(row); row = new List<Snapper>();
            }
            return board;
        }
    }

    public static class SnapperBuilder
    {
        public static Snapper makeSnapper(int x, int y, SnapperColor color)
        {
            return new Snapper
            {
                State = color,
                Pos = Tuple.Create<int, int>(y, x)
            };
        }
    }
}
