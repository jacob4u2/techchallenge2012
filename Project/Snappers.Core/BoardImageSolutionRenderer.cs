﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Snappers.Core
{
    public class BoardImageSolutionRenderer
    {
        // #7B9EC6
        private Color dotBg = Color.FromArgb(0x7B, 0x9E, 0xC6);

        public Bitmap RenderSolutionImage(Bitmap boardImg, List<Tuple<int, int>> solution)
        {   
            using (var copy = new Bitmap(boardImg))
            using (var whiteBrush = new SolidBrush(Color.White))
            using (var whitePen = new Pen(Color.White, 2.0f))
            using (var dotBrush = new SolidBrush(dotBg))
            using (var g = Graphics.FromImage(copy))
            {
                // TODO: Write a snapper position builder chooser that accounts for boardImg size.
                var positions = BoardImageSnapperPositionChooser.GetBuilder(boardImg).GetPositions();
                int i = 1;
                foreach (var step in solution)
                {
                    // Draw the circle
                    var position = positions[step.Item2][step.Item1];
                    var boundRect = Rectangle.FromLTRB(position.Item1 - 5, position.Item2 - 15, position.Item1 + 40, position.Item2 + 30);
                    g.FillEllipse(dotBrush, boundRect);
                    g.DrawEllipse(whitePen, boundRect);

                    var f = new Font(FontFamily.GenericSansSerif, 20.0f, FontStyle.Bold);
                    // Draw the text.
                    g.DrawString(i.ToString(), f, whiteBrush, new PointF(position.Item1+1, position.Item2-11));

                    i++;
                }

                g.Flush();

                return new Bitmap(copy);
            }            
        }

        
        
    }
}
