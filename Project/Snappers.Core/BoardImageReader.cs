﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Snappers.Core
{
    public abstract class BoardImageSnapperPositionBuilderBase
    {
        public abstract List<List<Tuple<int, int>>> GetPositions();

        protected List<List<Tuple<int, int>>> BuildPositionList(int rowHeight, int colWidth, int startRow, int startCol)
        {
            var snapPositions = new List<List<Tuple<int, int>>>();

            for (int r = 0; r < 6; r++)
            {
                var row = new List<Tuple<int, int>>();
                for (int c = 0; c < 5; c++)
                {
                    row.Add(Tuple.Create(startCol + (c * colWidth), startRow + (r * rowHeight)));
                }
                snapPositions.Add(row);
            }

            return snapPositions;
        }
    }

    public class BoardImageSnapperPositionIPadBuilder : BoardImageSnapperPositionBuilderBase
    {
        public override List<List<Tuple<int, int>>> GetPositions()
        {
            int rowHeight = 120, colWidth = 115, startRow = 240, startCol = 160;
            return base.BuildPositionList(rowHeight, colWidth, startRow, startCol);
        }
    }

    public class BoardImageSnapperPositionIPhoneHighBuilder : BoardImageSnapperPositionBuilderBase
    {
        public override List<List<Tuple<int, int>>> GetPositions()
        {
            int rowHeight = 133, colWidth = 127, startRow = 176, startCol = 62;
            return base.BuildPositionList(rowHeight, colWidth, startRow, startCol);
        }
    }

    public class BoardImageSnapperPositionIPhoneMidBuilder : BoardImageSnapperPositionBuilderBase
    {
        public override List<List<Tuple<int, int>>> GetPositions()
        {
            int rowHeight = 88, colWidth = 85, startRow = 120, startCol = 41;
            return base.BuildPositionList(rowHeight, colWidth, startRow, startCol);
        }
    }

    public class BoardImageSnapperPositionIPhoneLowBuilder : BoardImageSnapperPositionBuilderBase
    {
        public override List<List<Tuple<int, int>>> GetPositions()
        {
            int rowHeight = 44, colWidth = 42, startRow = 60, startCol = 21;
            return base.BuildPositionList(rowHeight, colWidth, startRow, startCol);
        }
    }

    public static class BoardImageSnapperPositionChooser
    {
        public static BoardImageSnapperPositionBuilderBase GetBuilder(Bitmap img)
        {
            var width = (int)img.PhysicalDimension.Width;

            if (width > 640)
                return new BoardImageSnapperPositionIPadBuilder();

            if (width > 426)
                return new BoardImageSnapperPositionIPhoneHighBuilder();

            if (width > 213)
                return new BoardImageSnapperPositionIPhoneMidBuilder();

            return new BoardImageSnapperPositionIPhoneLowBuilder();
        }
    }

    public class BoardImageReader
    {
        
        public SnapperBoard ReadBoard(string fileName, int numTaps = 6)
        {
            using (var img = (Bitmap)Bitmap.FromFile(fileName))
            {
                return ReadBoard(img, numTaps);
            }
        }

        public SnapperBoard ReadBoard(Bitmap img, int numTaps = 6)
        {
            var snapPositions = BoardImageSnapperPositionChooser.GetBuilder(img).GetPositions();
            
            List<string> rows = new List<string>();
            for (int i = 0; i < 6; i++)
            {
                string row = string.Empty;
                for (int j = 0; j < 5; j++)
                {
                    row += GetColorFromImagePosition(img, snapPositions[i][j]);
                }
                rows.Add(row);
            }

            return SnapperBoard.Make(string.Join("|", rows.ToArray()), numTaps);
        }

        private Dictionary<string, List<Color>> snapperColors = new Dictionary<string, List<Color>>
        {
            {"r", new List<Color>() { Color.FromArgb(0xCE, 0,0), Color.FromArgb(0xC6, 0,0), Color.FromArgb(0xCC, 0,0) }},
            {"g", new List<Color>() { Color.FromArgb(0x7B, 0xCF, 0x18), Color.FromArgb(0x84, 0xCF, 0x18), Color.FromArgb(0x81, 0xCF, 0x18), Color.FromArgb(0x80, 0xCF, 0x18) }},
            {"b", new List<Color>() { Color.FromArgb(0x39, 0x8A, 0xB5), Color.FromArgb(0x39, 0x8A, 0xAD), Color.FromArgb(0x3D, 0x8A, 0xB2), Color.FromArgb(0x3C, 0x8A, 0xB2), Color.FromArgb(0x3D, 0x8A, 0xB1), Color.FromArgb(0x3C, 0x8A, 0xB1) }},
            {"o", new List<Color>() { Color.FromArgb(0xFF, 0x9A, 0x00), Color.FromArgb(0xFF, 0x96, 0x00), Color.FromArgb(0xFF, 0x99, 0x00) }}
        };

        public string GetColorFromImagePosition(Bitmap img, Tuple<int, int> point)
        {
            // Check a 25x25 grid with point at the center.
            // TODO: May want to change the grid size based on img dimensions.
            int bottomLimit = GetGridSizeFromImg(img), topLimit = bottomLimit + 1;

            Color px;
            for (int i = point.Item1 - bottomLimit; i < point.Item1 + topLimit; i++)
            {
                for (int j = point.Item2 - bottomLimit; j < point.Item2 + topLimit; j++)
                {
                    px = img.GetPixel(i, j);
                    foreach(var keyVal in snapperColors)
                    {
                        if (keyVal.Value.Any(x => px.Equals(x)))
                            return keyVal.Key;
                    }
                }
            }

            return "_";
        }

        private int GetGridSizeFromImg(Bitmap img)
        {
            var width = img.Width;
            if (width > 600)
                return 25;

            if (width > 400)
                return 20;

            return 12;
        }
    }
}
