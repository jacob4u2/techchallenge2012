﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Snappers.Core
{
    public class SnapperBoard
    {
        public List<List<Snapper>> Snappers { get; set; }
        public List<Dart> Darts { get; set; }
        public int TouchesLeft { get; set; }

        public SnapperBoard()
        {
            Darts = new List<Dart>();
        }

        public Snapper this[int x, int y]
        {
            get
            {
                if (y > Snappers.Count-1 || x > Snappers[y].Count-1)
                    return null;

                return Snappers[y][x];
            }
            set
            {
                if (y > Snappers.Count-1 || x > Snappers[y].Count - 1)
                    return;

                Snappers[y][x] = value;
            }
        }

        public bool DoTouch(int x, int y)
        {
            TouchesLeft--;
            var startSnap = TouchSnapper(x, y);
            if (startSnap == null)
                return false;

            if (startSnap.State != SnapperColor.explode)
                return true;

            while (Darts.Count > 0)
            {
                // Move the Dart
                var currBoard = SnapperBoard.Clone(this);
                var toRemove = new List<Dart>();
                var darts = Darts.Select(d => d).ToList();
                foreach (var dart in darts)
                {
                    var onBoard = dart.Move();
                    if (!onBoard)
                    {
                        toRemove.Add(dart);
                        continue;
                    }

                    var hit = HandleDartMoved(dart);
                    // Switched to just check if a snapper is there so two darts hitting the same snapper are both removed.
                    if (currBoard[dart.CurrPos.Item1, dart.CurrPos.Item2] != null)
                        toRemove.Add(dart);
                }
                toRemove.ForEach(d => Darts.Remove(d));
            }

            return true;
        }

        public Snapper TouchSnapper(int x, int y)
        {
            var startSnap = this[x, y];
            if (startSnap == null)
                return null;

            var exploded = startSnap.incrementColor();
            if (!exploded)
                return startSnap;

            HandleExplodeSnapper(startSnap);
            
            return startSnap;
        }


        private bool HandleDartMoved(Dart dart)
        {
            var snap = this[dart.CurrPos.Item1, dart.CurrPos.Item2];
            if (snap == null)
                return false;

            var hit = TouchSnapper(dart.CurrPos.Item1, dart.CurrPos.Item2);

            if (hit != null)
                return true;

            return false;
        }

        private void HandleExplodeSnapper(Snapper startSnap)
        {
            // Create our initial darts in the same position
            for (int i = 0; i < 4; i++)
            {
                Darts.Add(new Dart
                    {
                        CurrPos = Tuple.Create(startSnap.Pos.Item1, startSnap.Pos.Item2),
                        StartPos = Tuple.Create(startSnap.Pos.Item1, startSnap.Pos.Item2),
                        Dir = (Direction)i
                    });
            }

            // Set original snapper to null.
            this[startSnap.Pos.Item1, startSnap.Pos.Item2] = null;
        }

        public bool IsWon()
        {
            // TODO: Check the board for winning state.
            return Snappers.All(x => x.All(y => y == null));
        }

        public static SnapperBoard Make(string board, int touches)
        {
            var builder = new BoardBuilder();
            var snaps = builder.ParseString(board);

            return new SnapperBoard()
            {
                Snappers = snaps,
                TouchesLeft = touches,
                Darts = new List<Dart>()
            };
        }

        public static SnapperBoard Make(List<List<Snapper>> snappers, int touches)
        {
            return new SnapperBoard
            {
                Snappers = snappers,
                TouchesLeft = touches
            };
        }

        public override string ToString()
        {
            return string.Join("|", this.Snappers.Select(x => string.Concat(x.Select(y => y == null ? "_" : y.State.ToString()[0].ToString()))).ToArray());
        }

        public static SnapperBoard Clone(SnapperBoard orig)
        {
            return Make(orig.ToString(), orig.TouchesLeft);
        }

        public List<Snapper> GetEligibleSnappers()
        {
            if (this.TouchesLeft < 1)
                return new List<Snapper>();

            var tl = this.TouchesLeft;
            return (tl == 1) ?
                    this.GetReds() :
                        (tl == 2) ?
                            this.GetRedGreens() :
                            (tl == 3) ?
                            this.GetRedsGreenOranges() :
                                this.GetAll();
        }

        public List<Snapper> GetReds()
        {
            return CommonGet(SnapperColor.red);
        }

        public List<Snapper> GetRedGreens()
        {
            return CommonGet(SnapperColor.green);            
        }

        public List<Snapper> GetRedsGreenOranges()
        {
            return CommonGet(SnapperColor.orange);
        }

        public List<Snapper> GetAll()
        {
            return CommonGet(SnapperColor.blue);
        }

        private List<Snapper> CommonGet(SnapperColor c)
        {
            return Snappers.SelectMany(x => x.Where(y => y != null && y.State >= c)).ToList();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return GetHashCode() == ((SnapperBoard)obj).GetHashCode();
        }

        public override int GetHashCode()
        {
            return ("[" + this.TouchesLeft + "] " + this.ToString()).GetHashCode();
        }
    }
}
